#include<stdio.h>
#include<stdlib.h>

int histogram(double* m, long n);
int sn_kat(double* m, double* x, double* y,double* z, double* xv, double* yv, double* zv,long n);
int gw_kat(double* m, double* x, double* y,double* z, double* xv, double* yv, double* zv,long n);
int position(double* x, double* y , double* z,double* m, long n);


int main(int argc, char** argv)
{

const long n=100000000;
long i=0;
long SIZE = 0;

double* m = malloc(n * sizeof(double));
double* x = malloc(n * sizeof(double));
double* y = malloc(n * sizeof(double));
double* z = malloc(n* sizeof(double));
double* xv = malloc(n * sizeof(double));
double* yv = malloc(n * sizeof(double));
double* zv = malloc(n * sizeof(double));
double* m1 = malloc(n * sizeof(double));
double* x1 = malloc(n * sizeof(double));
double* y1 = malloc(n * sizeof(double));
double* z1 = malloc(n* sizeof(double));
double* xv1 = malloc(n * sizeof(double));
double* yv1 = malloc(n * sizeof(double));
double* zv1 = malloc(n * sizeof(double));
double mass, xpos, ypos, zpos, xvel, yvel, zvel;

FILE* fh_in;
char fname[100];
char ignore[1000];


//set the number of file used
for(int l=0;l<=15;l++){

sprintf(fname,"halos_15.%d.ascii",l);

if(!(fh_in = fopen(fname, "r")))//If the file cant be open a statement is printet
{
   printf("can't read file '%s'\n", fname);
   return 1;
}

else{
	while(fgets(ignore,sizeof(ignore),fh_in)!=NULL){
	if(ignore[0] == '#')continue; //Does not read the lines that start with a #	
	

       while(fscanf(fh_in, "%*s %*s %lg %*s %*s %*s %*s %*s %lg %lg %lg %lg %lg %lg %*[^\n]",&mass,&xpos,&ypos,&zpos,&xvel,&yvel,&zvel) > 0) //only using third value
    {
      m[i] = mass; m1[i] = mass;
      x[i] = xpos; x1[i] = xpos;
      y[i] = ypos; y1[i] = ypos;
      z[i] = zpos; z1[i] = zpos;    
      xv[i] = xvel; xv1[i] = xvel;
      yv[i] = yvel; yv1[i] = yvel;
      zv[i] = zvel; zv1[i] =zvel;
	i++;
   }
	
}
}

SIZE = i;
fclose(fh_in);
}
// Only one of the functions below is used at a time

//position(&x[0],&y[0],&z[0],&m[0],SIZE);
//histogram(&m[0],SIZE);
//sn_kat(&m[0],&x[0],&y[0],&z[0],&xv[0],&yv[0],&zv[0],SIZE);
gw_kat(&m1[0],&x1[0],&y1[0],&z1[0],&xv1[0],&yv1[0],&zv1[0],SIZE);

free(m); free(m1);
free(x); free(x1);
free(y); free(y1);
free(z); free(z1);
free(xv); free(xv1);
free(yv); free(yv1);
free(zv); free(zv1);
return 0;
}


#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<string.h>
#define RND (double)rand()/RAND_MAX

int sn_kat(double* m, double* x, double* y, double* z,double* xvel, double* yvel, double* zvel,long size){

int i;
double tot_val;
const long n=10000000000; 

//srand(1000); //used for ramdomnumbers
long double* H_std = malloc(n * sizeof(double));
long double* mas_bin = malloc(n * sizeof(double));
long double* line_mass = malloc(n * sizeof(double));
long double* sn_list = malloc(n * sizeof(double));
long double* m_new = malloc(n * sizeof(double)); 
long double* distance = malloc(n * sizeof(double));
long double* velocity = malloc(n * sizeof(double));
long double* H_loc = malloc(n* sizeof(double));
long double* vr = malloc(n * sizeof(double));
long double* x_new = malloc(n * sizeof(double));
long double* y_new = malloc(n * sizeof(double));
long double* z_new = malloc(n * sizeof(double));
long double* xvel_new = malloc(n * sizeof(double));
long double* yvel_new = malloc(n * sizeof(double));
long double* zvel_new = malloc(n * sizeof(double));
long double* v = malloc(n * sizeof(double));
long double* dist = malloc(n * sizeof(double)); 
long double* hast = malloc(n * sizeof(double));
long double* dist_norm = malloc(n* sizeof(double));
long double* H_no_error = malloc(n* sizeof(double));
long double* H_loc_uden = malloc(n* sizeof(double));

long sn_num = 1000; // number of supernova
double box_size = 512; 

int obs_xpos = 50, obs_ypos = 100 , obs_zpos = 239; // posistion of observer
int radius = 200;//Radius of the sphere in Mpc
long size_rad = 0; // Size of values within the sphere

//the next section open and reads to file.
char mode[2], filename[200];

FILE* fh; //file handler

strcpy(mode, "w"); // Create a new file. If the file already exists it is overwritten
sprintf(filename,"SN.data.txt");

if(!(fh = fopen(filename, mode)))
{
printf("error in opening file '%s'\n", filename);

}

//All values in the data is plussed in tot_val
//If the observer is in a corner of the box, and a values is outside the box, the box will repeat it self. 
	for(i=0;i<size;i++)
{
	if(fabs(obs_xpos-x[i]) < fabs(obs_xpos-(x[i]-box_size)) && fabs(obs_xpos-x[i]) < fabs(obs_xpos-(x[i]+box_size))){ 
		x[i] = x[i];}
	else if (fabs(obs_xpos-(x[i]-box_size)) < fabs(obs_xpos-(x[i]+box_size)) && fabs(obs_xpos-(x[i]-box_size)) < fabs(obs_xpos-x[i])){
		x[i] = x[i]-box_size;}
	else if(fabs(obs_xpos-(x[i]+box_size)) < fabs(obs_xpos-(x[i]-box_size)) && fabs(obs_xpos-(x[i]+box_size)) < fabs(obs_xpos-x[i])){
		 x[i] = x[i]+box_size;}
	else x[i]=0;

        if(fabs(obs_ypos-y[i]) < fabs(obs_ypos-(y[i]-box_size)) && fabs(obs_ypos-y[i]) < fabs(obs_ypos-(y[i]+box_size))){
                y[i] = y[i];}
        else if (fabs(obs_ypos-(y[i]-box_size)) < fabs(obs_ypos-(y[i]+box_size)) && fabs(obs_ypos-(y[i]-box_size)) < fabs(obs_ypos-y[i])){
	        y[i] = y[i]-box_size;}
        else if(fabs(obs_ypos-(y[i]+box_size)) < fabs(obs_ypos-(y[i]-box_size)) && fabs(obs_ypos-(y[i]+box_size)) < fabs(obs_ypos-y[i])){
               y[i] = y[i]+box_size;}
        else y[i]=0; 

        if(fabs(obs_zpos-z[i]) < fabs(obs_zpos-(z[i]-box_size)) && fabs(obs_zpos-z[i]) < fabs(obs_zpos-(z[i]+box_size))){
                z[i] = z[i];}
        else if (fabs(obs_zpos-(z[i]-box_size)) < fabs(obs_zpos-(z[i]+box_size)) && fabs(obs_zpos-(z[i]-box_size)) < fabs(obs_zpos-z[i])){
        z[i] = z[i]-box_size;}
        else if(fabs(obs_zpos-(z[i]+box_size)) < fabs(obs_zpos-(z[i]-box_size)) && fabs(obs_zpos-(z[i]+box_size)) < fabs(obs_zpos-z[i])){
         z[i] = z[i]+box_size;}
        else z[i]=0;
 
      if(sqrt(pow(x[i]-obs_xpos,2)+pow(y[i]-obs_ypos,2)+pow(z[i]-obs_zpos,2)) < radius){   //specifies in with area the observer are looking
		tot_val += m[i];
		size_rad++;
		m_new[size_rad] = m[i];
		x_new[size_rad] = x[i];
		y_new[size_rad] = y[i];
		z_new[size_rad] = z[i];
		xvel_new[size_rad] = xvel[i];
		yvel_new[size_rad] = yvel[i];
		zvel_new[size_rad] = zvel[i];
//	fprintf(fh,"%g",x[i]);
}
	else continue;
}
//Finding a interval equivalent to the halo mass size.
//Scaling it down to 0-1. 
        for(i=0;i<size_rad;i++)
{
        	mas_bin[i] = m_new[i]/tot_val;
        
		if(i==0)
{
			line_mass[i] += mas_bin[i]  ;
}
		else
{
			line_mass[i] += mas_bin[i]+line_mass[i-1];
}
}

//Randomly placing SN in the haloes, depending on their size. Binary search is used.
	for(int t=0;t<sn_num;t++)
{
		long double k = RND;

		int first = 0, last = size_rad-1, middle = (first+last)/2;
		while(first <= last){
		if(k<line_mass[0]){
			sn_list[0] +=1;
			fprintf(fh,"%LF %d %LF\n",k,1,sn_list[0]);
}
		else if(k>line_mass[middle]){
				if(k<=line_mass[middle+1]){
					sn_list[middle] += 1;
			             velocity[t] = sqrt(pow(xvel_new[middle],2)+pow(yvel_new[middle],2)+pow(zvel_new[middle],2));
			distance[t] = sqrt(pow(x_new[middle]-obs_xpos,2)+pow(y_new[middle]-obs_ypos,2)+pow(z_new[middle]-obs_zpos,2));

//Box Muller tansformation for normaldestribution
//we want a random number for -1 to 1
		long double s = 0;
		long double z = 0, rand1 = 0.0, rand2 = 0.0;
		long double sigma = 0.1;
		while(s>=1.0 || s==0.0){

		 rand1 = 2.0 * RND - 1;
		 rand2 = 2.0 * RND - 1; 
		 s = rand1*rand1+rand2*rand2;
		}
	
		z = 1 + sigma * rand1 * sqrt( - 2.0 * log(s) / s);
		dist_norm[t] = distance[t]*z;

//Projektion of the vecter, in line of sight.
			dist[t] = sqrt(pow((x_new[middle]-obs_xpos),2)+pow(y_new[middle]-obs_ypos,2)+pow(z_new[middle]-obs_zpos,2));
			                long x_sight = xvel_new[middle] * ((x_new[middle]-obs_xpos)/dist[t]);
					long y_sight = yvel_new[middle] * ((y_new[middle]-obs_ypos)/dist[t]);	
				        long z_sight = zvel_new[middle] * ((z_new[middle]-obs_zpos)/dist[t]);
					hast[t] = x_sight + y_sight + z_sight;

					vr[t] = 100 * distance[t];
					v[t] = vr[t] + hast[t];
					H_loc[t] = v[t]/dist_norm[t];
					H_loc_uden[t] = v[t]/distance[t];
					
fprintf(fh,"%LF %LF %LF %LF %LF %d %LF %LF %LF\n",H_loc[t],vr[t],(vr[t]-v[t]/dist_norm[t]),dist_norm[t],z,middle,sn_list[middle],distance[t],H_loc_uden[t]);

break;}
					 else first = middle + 1;}
		else 
		last = middle -1;
		middle = (first+last)/2 ;
}}
	fprintf(fh,"\n\n");

long double H_loc_mean,  mean_int, std;
double total_sn_num=0.0;
//Mean value for H0 pr 4 Mpc
//the uncententies is calculated from the standartdeviation

for(int k=4; k <= 180 ; k += 4)
{
	H_std[i] = 0.0;
        H_loc_mean = 0.0;
        mean_int = 0.0;
        std = 0.0;
        double number = 0.0;
	int p=0;
	for(int i=0; i<sn_num ;i ++)
{       

	if(dist_norm[i]<=k && dist_norm[i]> k-4)
{
	number++;
	H_loc_mean += H_loc[i];	
	H_std[p] = H_loc[i];
	p++;
}
}
	total_sn_num +=number;
	mean_int = H_loc_mean/number;// H mean value in the given interval.
	for(int o = 0; o<number;o++){
	std += pow((number-1),-1)*pow((H_std[o]-mean_int),2); 
 }
fprintf(fh,"%g  %d %LF  %g \n",number,k,mean_int,sqrt(std));

}
fprintf(fh,"\n\n");
fprintf(stdout,"Antal SN i interval = %g",total_sn_num);
fprintf(fh,"\n\n");

int count = 0.0;
double mean_tot =0.0, total_value  = 0.0, std_tot = 0.0;
for(int i=0; i <sn_num ; i ++)
{
	if(dist_norm[i]<=180){
	count++;
	total_value += H_loc[i];}
	else continue;
}

 mean_tot = total_value/count;

for(int i = 0; i<count;i++){
    std_tot += pow((count-1),-1)*pow((H_loc[i]-mean_tot),2);}


fprintf(stdout,"Mean H0 = %g STD = %g\n", mean_tot,sqrt(std_tot));

fprintf(fh,"%d %d\n",180,0);
fprintf(fh,"%d %d\n",180,140);



fclose(fh);

free(x_new); free(y_new); free(z_new); free(xvel_new); free(yvel_new); free(zvel_new);
free(vr); free(v); free(H_std);
free(H_loc);free(hast);free(H_no_error);
free(distance); free(dist); free(dist_norm);
free(m_new); free(H_loc_uden);
free(mas_bin);
free(line_mass);
free(sn_list);
free(velocity);
return 0;
}


#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

int position(double* x, double* y, double* z,double* m, long SIZE){

int i, x_val = 0, y_val = 0 ;
int number = 1;
const double  arr_size = 512/number;
double** matrix;
matrix = calloc(arr_size, sizeof(double*));

for(i =0; i<arr_size;i++){
	matrix[i] = calloc(arr_size, sizeof(double));
}

	for(i=0;i<SIZE;i++)
{

	if(z[i]>100 && z[i]<130 ){
		if(m[i]>pow(10,14)){
			m[i]= pow(10,13);}
		else m[i]=m[i];

	x_val = x[i]/number;
	y_val = y[i]/number;
	matrix[x_val][y_val] += m[i]; 

	}	

	else continue;
 }



char mode[2], filename[200];

FILE* fh; //file handler

strcpy(mode, "w"); /* Create a new file. If the file already exists it is overwritten.*/
sprintf(filename,"pos1.data.txt");



if(!(fh = fopen(filename, mode)))
{
printf("error in opening file '%s'\n", filename);

}

for(i = 0; i< arr_size; i++){
	fprintf(fh,"\n\n");
	for(int j = 0; j < arr_size;j++){
		fprintf(fh,"%d %d %f\n",i,j,pow(matrix[i][j],0.2));}}
		
fclose(fh);

for(i =0; i < arr_size; i++)
	free(matrix[i]);
free(matrix);        

return 0;

}




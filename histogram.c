#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

int histogram(double* x, long SIZE)
{

double bin_size = 10e11;
double bin_num1;
int i, bin_num;

const long arr_size = 10000;
long arr[arr_size]; 

	for (i = 0; i < arr_size; ++i)
   		arr[i] = 0;


	for(i=0;i<SIZE;i++)
{
    		bin_num1 = floor(x[i] / bin_size);
    		bin_num = bin_num1;
	if (bin_num >= arr_size)
       	continue;
   
    		arr[bin_num] += 1;
}

char mode[2], filename[200];

FILE* fh; //file handler

strcpy(mode, "w"); /* Create a new file. If the file already exists it is overwritten.*/
sprintf(filename,"plot.data.txt");



if(!(fh = fopen(filename, mode)))
{
printf("error in opening file '%s'\n", filename);

}

//fprintf(fh, "%ld\n\n", SIZE); 

for(i=0;i<arr_size;i++)
	 fprintf(fh, "%f %ld \n",(i+1)*bin_size, arr[i]);

fclose(fh);

return 0;

}



